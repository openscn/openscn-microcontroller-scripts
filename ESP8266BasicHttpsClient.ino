/**
   BasicHTTPSClient.ino
    Created on: 20.08.2018

*/

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#include <WiFiClientSecureBearSSL.h>

// Fingerprint for demo URL, expires on June 2, 2019, needs to be updated well before this date
uint8_t jigsawFingerprint[20] = {0x5A, 0xCF, 0xFE, 0xF0, 0xF1, 0xA6, 0xF4, 0x5F, 0xD2, 0x11, 0x11, 0xC6, 0x1D, 0x2F, 0x0E, 0xBC, 0x39, 0x8D, 0x50, 0xE0};

//WiFiMulti instance, on setup you can enter many different APs' credentials
ESP8266WiFiMulti WiFiMulti;

//Store GET Request's response payload
String payload;

// FUNCTIONS
int getRequestSecure(String URL, uint8_t *fingerprint, String &payload);
int getRequest(String URL, String &payload);
int postRequestSecure(String URL, uint8_t *fingerprint, String data, String contentType = "text/plain");
int postRequest(String URL, String data, String contentType = "text/plain");
void sleepSeconds(int seconds);

void setup() {

  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  //WifiMulti let's you add many wifi APs credentials and connects to the one available
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("SSID", "PASSWORD");
}

void loop() {
  // wait for WiFi connection
  if ((WiFiMulti.run() != WL_CONNECTED)) {
    Serial.println("Connecting to WiFi");
  }else{
    getRequestSecure("https://jigsaw.w3.org/HTTP/connection.html", jigsawFingerprint, payload);
  }

  Serial.println("Wait 10s before next round...");
  delay(10000);
}

//returns HTTP code
int getRequestSecure(String URL, uint8_t *fingerprint, String &payload){

  int httpCode; //HTTP response code

  std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);  //establish secure connection

  client->setFingerprint(fingerprint);  //set Server's fingerprint

  HTTPClient https; //Connection instance

  Serial.print("[HTTPS] begin...\n");
  if (https.begin(*client, URL)) {  //Successful connection

    Serial.print("[HTTPS] GET...\n");
    httpCode = https.GET(); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        payload = https.getString();
        //Serial.println(payload);
      }
    } else {
      Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
    }

    https.end();
    return httpCode;
  } else {
    Serial.printf("[HTTPS] Unable to connect\n");
    return 404;
  }
}


int getRequest(String URL, String &payload){

  int httpCode; //HTTP response code

  HTTPClient http; //Connection instance

  Serial.print("[HTTP] begin...\n");
  if (http.begin(URL)) {  //Successful connection

    Serial.print("[HTTP] GET...\n");
    httpCode = http.GET(); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        payload = http.getString();
        //Serial.println(payload);
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return httpCode;
  } else {
    Serial.printf("[HTTP] Unable to connect\n");
    return 404;
  }
}

int postRequestSecure(String URL, uint8_t *fingerprint, String data, String contentType){

  int httpCode; //HTTP response code

  std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);  //establish secure connection

  client->setFingerprint(fingerprint);  //set Server's fingerprint

  HTTPClient https; //Connection instance

  Serial.print("[HTTPS] begin...\n");
  if (https.begin(*client, URL)) {  //Successful connection

    https.addHeader("Content-Type", contentType); //Header to change the content type if JSON is POSTED

    Serial.print("[HTTPS] POST...\n");
    Serial.println(data);
    httpCode = https.POST(data); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTPS] POST... code: %d\n", httpCode);

    } else {
      Serial.printf("[HTTPS] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
    }

    https.end();
    return httpCode;
  } else {
    Serial.printf("[HTTPS] Unable to connect\n");
    return 404;
  }
}

int postRequest(String URL, String data, String contentType){

  int httpCode; //HTTP response code

  HTTPClient http; //Connection instance

  Serial.print("[HTTP] begin...\n");
  if (http.begin(URL)) {  //Successful connection

    http.addHeader("Content-Type", contentType); //Header to change the content type if JSON is POSTED

    Serial.print("[HTTP] POST...\n");
    Serial.println(data);
    httpCode = http.POST(data); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return httpCode;
  } else {
    Serial.printf("[HTTP] Unable to connect\n");
    return 404;
  }
}

//Maximum time of sleep cannot exceed 71 minutes because of integer overflow
void sleepMinutes(int minutes){
  ESP.deepSleep(minutes * 60000000);
}
